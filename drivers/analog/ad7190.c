/****************************************************************************
 * drivers/sensors/AD7190.c
 * Character driver for the AD7190
 *
 * Copyright (C) 2015, 2017 Gregory Nutt. All rights reserved.
 * Based on driver MAX31855 from Alan Carvalho de Assis <acassis@gmail.com>>
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdlib.h>
#include <fixedmath.h>
#include <errno.h>
#include <debug.h>

#include <nuttx/kmalloc.h>
#include <nuttx/fs/fs.h>
#include <nuttx/spi/spi.h>
#include <nuttx/analog/ad7190.h>

#ifdef CONFIG_SPI && CONFIG_ADC_AD7190

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/****************************************************************************
 * Private
 ****************************************************************************/

struct ad7910_dev_s
{
  FAR struct spi_dev_s *spi;	/* SPI struct vor SPI Communication*/
  FAR int devicid;				/* deviced ID for SPI_SELECT*/
  sem_t exclsem;           		/* exclusion *//* Saved SPI driver instance */
};


/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Character driver methods */

static int     ad7190_open(FAR struct file *filep);
static int     ad7190_close(FAR struct file *filep);
static ssize_t ad7190_read(FAR struct file *filep, FAR char *buffer,
                             size_t buflen);
static ssize_t ad7190_write(FAR struct file *filep, FAR const char *buffer, size_t buflen);

static int     ad7190_ioctl(FAR struct file *filep, int cmd, unsigned long arg);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct file_operations g_ad7910fops =
{
  ad7190_open,
  ad7190_close,
  ad7190_read,
  ad7190_write,
  NULL,
  ad7190_ioctl,    /* ioctl */
};

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad7190_open
 *
 * Description:
 *   This function is called whenever the AD5662 device is opened.
 *
 ****************************************************************************/

static int ad7190_open(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad7190_close
 *
 * Description:
 *   This routine is called when the AD5662 device is closed.
 *
 ****************************************************************************/

static int ad7190_close(FAR struct file *filep)
{
	return OK;
}

/****************************************************************************
 * Name: ad7190_read
 ****************************************************************************/

static ssize_t ad7190_read(FAR struct file *filep, FAR char *buffer, size_t buflen)
{
	return OK;
}

/****************************************************************************
 * Name: ad7190_write
 *
 * based on spi_transfer
 * Input Parameters:
 *   filep - file descriptor
 *   buffer -pointer to Byte Array, which contains all data to send
 *   size - size form 2 to 3 Byte
 *
 *
 ****************************************************************************/

static ssize_t ad7190_write(FAR struct file *filep, FAR const char *buffer, size_t buflen)
{
	return 0;
}

/****************************************************************************
 * Name: ad7190_ioctl
 *
 * Description:
 *
 *
 * Input Parameters:
 *   filep -
 *   cmd - IOCTL command
 *   arg - must be empty
 *
 *
 * Returned Value:
 *   Zero (OK) on success; a negated errno value on failure.
 *
 ****************************************************************************/


static int ad7190_ioctl (FAR struct file *filep, int cmd, unsigned long arg)
{

	FAR struct inode *inode = filep->f_inode;
	FAR struct ad7910_dev_s *priv;
	int ret;
	int i = 0;

	char *buffer;
	buffer = &arg;

	/* Get exclusive access to the SPI driver state structure */

	DEBUGASSERT(filep != NULL && filep->f_inode != NULL);
	inode = filep->f_inode;

	priv = (FAR struct ad5662_dev_s*)inode->i_private;
	DEBUGASSERT(priv);


	/*Fill Buffer with dummy data*/
	buffer = 0xFF;
	buffer ++;
	buffer = 0xFF;
	buffer ++;
	buffer = 0xFF;

	/*set buffer to start adress*/
	buffer = &arg;

	ret = sem_wait(&priv->exclsem);

	if (ret < 0)
	{
		int errcode = errno;
		DEBUGASSERT(errcode < 0);
		return -errcode;
	}


	switch(cmd){
		/*SPI Initialization
		 *set all settings
		 */
		case 0:
			sem_post(&priv->exclsem);
			SPI_LOCK(priv->spi, true);
			SPI_SETFREQUENCY(priv->spi,1500);
			SPI_SETMODE(priv->spi,SPIDEV_MODE3);
			SPI_SETBITS(priv->spi,8);
			SPI_LOCK(priv->spi, false);
			sem_post(&priv->exclsem);

		break;
		/*AD7190 Reset
		 *
		 */
		case 1:
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, priv->devicid, true);

			for(i=0;i<10;i++){
				SPI_SEND(priv->spi,0xFFFF);
			}

			SPI_SELECT(priv->spi, priv->devicid, false);

			SPI_LOCK(priv->spi, false);

			sem_post(&priv->exclsem);
		break;
		/*continuous
		 *
		 * not ready*/
		case 2:
			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, priv->devicid, true);

			SPI_SEND(priv->spi,0x5c);

			SPI_SELECT(priv->spi, priv->devicid, false);

			SPI_LOCK(priv->spi, false);

			//sem_post(&priv->exclsem);
		break;
		/*read*/
		case 3:
			buffer =&arg;

			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, priv->devicid, true);

			SPI_SEND(priv->spi,0x60);

			SPI_RECVBLOCK(priv->spi,buffer,1);

			SPI_SELECT(priv->spi, priv->devicid, false);
			SPI_LOCK(priv->spi, false);
		break;
		/*AD7190 Read ID Register*/
		case 4:
			buffer =&arg;

			SPI_LOCK(priv->spi, true);
			SPI_SELECT(priv->spi, priv->devicid, true);

			SPI_SEND(priv->spi,0x60);

			SPI_RECVBLOCK(priv->spi,buffer,1);

			SPI_SELECT(priv->spi, priv->devicid, false);
			SPI_LOCK(priv->spi, false);
		break;
	}
	return 0;
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ad5662_register
 *
 * Description:
 *   Register the ad5662 character device as 'devpath'
 *
 * Input Parameters:
 *   devpath - The full path to the driver to register. E.g., "/dev/temp0"
 *   spi - An instance of the SPI interface to use to communicate wit
 *   bus - the bus adress of the connected SPI Device
 *
 *
 * Returned Value:
 *   Zero (OK) on success;
 *
 ****************************************************************************/

int ad7190_register(FAR const char *devpath,FAR struct spi_dev_s *spi, int num_dev)
{
	FAR struct ad7910_dev_s *priv;
	int ret = 0;

	priv = (FAR struct ad7910_dev_s *)kmm_zalloc(sizeof(struct ad7910_dev_s));

	/*check*/
	priv->spi = spi;
	priv->exclsem.semcount = 1;
	priv->devicid = num_dev;

	ret = register_driver(devpath, &g_ad7910fops, 0666, priv);

	return ret;
}
#endif /* CONFIG_SPI && CONFIG_AD5662 */
